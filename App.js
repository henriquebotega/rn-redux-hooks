import React from 'react';

import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import {Provider} from 'react-redux';
import store from './src/redux/store';
import ViewNotes from './src/ViewNotes';
import AddNotes from './src/AddNotes';

class App extends React.Component {
  render() {
    const Stack = createStackNavigator();

    return (
      <>
        <Provider store={store}>
          <NavigationContainer>
            <Stack.Navigator initialRouteName="ViewNotes">
              <Stack.Screen name="ViewNotes" component={ViewNotes} />
              <Stack.Screen name="AddNotes" component={AddNotes} />
            </Stack.Navigator>
          </NavigationContainer>
        </Provider>
      </>
    );
  }
}

export default App;
