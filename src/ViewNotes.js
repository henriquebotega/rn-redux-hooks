import React from 'react';
import {StyleSheet, View, Text, FlatList, TouchableOpacity} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {addnote, deletenote} from './redux/actions';

function ViewNotes({navigation}) {
  const notes = useSelector(state => state);
  const dispatch = useDispatch();

  const addNote = note => dispatch(addnote(note));
  const deleteNote = id => dispatch(deletenote(id));

  return (
    <>
      <View style={styles.container}>
        {notes.length === 0 ? (
          <View style={styles.titleContainer}>
            <Text style={styles.title}>You do not have any notes</Text>
          </View>
        ) : (
          <FlatList
            data={notes}
            renderItem={({item}) => (
              <View key={item} style={styles.form}>
                <Text>{item.note.title}</Text>
                <Text>{item.note.description}</Text>

                <TouchableOpacity
                  style={styles.btnDel}
                  onPress={() => deleteNote(item.id)}>
                  <Text style={styles.btnText}>Del</Text>
                </TouchableOpacity>
              </View>
            )}
            keyExtractor={item => item.id.toString()}
          />
        )}
        <TouchableOpacity
          style={styles.fab}
          label="Add new note"
          onPress={() =>
            navigation.push('AddNotes', {
              addNote,
            })
          }>
          <Text style={styles.btnText}>Add</Text>
        </TouchableOpacity>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingHorizontal: 10,
    paddingVertical: 20,
  },
  titleContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  title: {
    fontSize: 20,
  },
  fab: {
    position: 'absolute',
    margin: 20,
    right: 0,
    bottom: 10,
    backgroundColor: 'darkgreen',
    padding: 10,
  },
  listTitle: {
    fontSize: 20,
  },
  form: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  btnDel: {
    backgroundColor: 'darkred',
    margin: 10,
    padding: 10,
  },
  btnText: {
    color: 'white',
  },
});

export default ViewNotes;
