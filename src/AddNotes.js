import React, {useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  StyleSheet,
} from 'react-native';

import {useDispatch} from 'react-redux';
import {addnote} from './redux/actions';

function AddNotes({navigation}) {
  const dispatch = useDispatch();

  const addNote = note => {
    dispatch(addnote(note));
    navigation.goBack();
  };

  const [noteTitle, setNoteTitle] = useState('');
  const [noteValue, setNoteValue] = useState('');

  return (
    <>
      <View style={styles.container}>
        <TextInput
          value={noteTitle}
          onChangeText={t => setNoteTitle(t)}
          style={styles.input}
        />
        <TextInput
          value={noteValue}
          onChangeText={t => setNoteValue(t)}
          style={styles.input}
        />

        <TouchableOpacity
          style={styles.btnAdd}
          onPress={() => addNote({title: noteTitle, description: noteValue})}>
          <Text style={styles.btnText}>Save</Text>
        </TouchableOpacity>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingHorizontal: 10,
    paddingVertical: 20,
  },
  input: {
    flexDirection: 'row',
    borderWidth: 1,
    margin: 10,
    borderColor: 'gray',
  },
  btnAdd: {
    backgroundColor: 'darkgreen',
    margin: 10,
    padding: 10,
    width: 55,
  },
  btnText: {
    color: 'white',
  },
});

export default AddNotes;
