export const ADD_NOTE = 'ADD_NOTE';
export const DEL_NOTE = 'DEL_NOTE';

let noteID = 0;

export function addnote(note) {
  return {
    type: ADD_NOTE,
    id: noteID++,
    note,
  };
}

export function deletenote(id) {
  return {
    type: DEL_NOTE,
    payload: id,
  };
}
