import {ADD_NOTE, DEL_NOTE} from './actions';

const initialState = [];

function myReducer(state = initialState, action) {
  switch (action.type) {
    case ADD_NOTE:
      return [
        ...state,
        {
          id: action.id,
          note: action.note,
        },
      ];

    case DEL_NOTE:
      return [...state.filter(i => i.id !== action.payload)];

    default:
      return state;
  }
}

export default myReducer;
